#!/usr/bin/env python

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

class Tree: pass

apple_tree = Tree()


class Cosmetics: pass

brush = Cosmetics()


class Cake: pass

apple_pie = Cake()


print(apple_tree , brush , apple_pie)
